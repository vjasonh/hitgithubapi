package com.example.hitgithubapi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hitgithubapi.databinding.ActivityMainBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String BASE_URL = "https://api.github.com/search/users?q=";
    private RecyclerView.Adapter adapter;

    private List<Data> list;

    ActivityMainBinding binding;
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    EditText search;
    String searchInput;
    int page = 1;

    int currItems, totalItems, scrolledOutItems, prevTotal;
    private boolean isLoading = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);

        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.custom_search, null);

        actionBar.setCustomView(v);

        list = new ArrayList<>();

        adapter = new Adapter(list, getApplicationContext());
        binding.recyclerView.setAdapter(adapter);

        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(linearLayoutManager);

        binding.welcomeMsg.setVisibility(View.VISIBLE);
        binding.noData.setVisibility(View.GONE);
        binding.loading.setVisibility(View.GONE);
        binding.maxHit.setVisibility(View.GONE);

        list = new ArrayList<>();

        search = (EditText) findViewById(R.id.search);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchInput = charSequence.toString();
                page = 1;
                currItems = 0;
                prevTotal = 0;
                totalItems = 0;
                scrolledOutItems = 0;

                if(binding.welcomeMsg.getVisibility() == View.VISIBLE){
                    binding.welcomeMsg.setVisibility(View.GONE);
                }
                binding.maxHit.setVisibility(View.GONE);
                loadData(searchInput);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void pagination(final String search){
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy>0){
                    currItems = linearLayoutManager.getChildCount();
                    totalItems = linearLayoutManager.getItemCount();
                    scrolledOutItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if(isLoading){
                        if(totalItems>prevTotal){
                            prevTotal = totalItems;
                            page++;
                            isLoading = false;
                        }
                    }

                    if(!isLoading && (scrolledOutItems+currItems) >= totalItems){
                        binding.loading.setVisibility(View.VISIBLE);
                        loadMoreData(search);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadData(String search) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL + search,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        list.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("items");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                Data data = new Data(
                                        obj.getString("login"),
                                        obj.getString("avatar_url"));

                                list.add(data);
                            }
                            if (jsonArray.length() == 0) {
                                binding.noData.setVisibility(View.VISIBLE);
                                binding.recyclerView.setVisibility(View.GONE);
                            } else {
                                binding.noData.setVisibility(View.GONE);
                                binding.recyclerView.setVisibility(View.VISIBLE);
                            }
                            adapter = new Adapter(list, getApplicationContext());
                            binding.recyclerView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        binding.noData.setVisibility(View.GONE);
                        binding.recyclerView.setVisibility(View.GONE);
                        binding.loading.setVisibility(View.GONE);
                        binding.maxHit.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), "Maximum API rate limit reached!", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        pagination(search);
    }

    private void loadMoreData(String search) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL+search+"&page="+page,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                binding.loading.setVisibility(View.GONE);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONArray jsonArray = jsonObject.getJSONArray("items");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject obj = jsonArray.getJSONObject(i);
                                        Data data = new Data(
                                                obj.getString("login"),
                                                obj.getString("avatar_url"));

                                        if (data.equals(null)) {
                                            binding.noData.setVisibility(View.VISIBLE);
                                            binding.recyclerView.setVisibility(View.INVISIBLE);
                                        } else {
                                            binding.noData.setVisibility(View.INVISIBLE);
                                            binding.recyclerView.setVisibility(View.VISIBLE);
                                        }
                                        list.add(data);
                                    }
                                    adapter.notifyDataSetChanged();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },2000);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        binding.noData.setVisibility(View.GONE);
                        binding.recyclerView.setVisibility(View.GONE);
                        binding.loading.setVisibility(View.GONE);
                        binding.maxHit.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), "Maximum API rate limit reached!", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}